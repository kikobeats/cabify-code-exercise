# Cabify Code Exercise

## RUN

```bash
npm install && npm test
```

## Author's considerations:

* Prefer pass a object argument instead of values because is more semantic. For example:

```coffee
new Item
  name: 'VOUCHER'
  description: 'Cabify Voucher'
  price: euro: 5.00
  discount:
    type: 'free units'
    rule: minimum: 2, free: 1
```

instead of

```coffee
new Item 'VOUCHER', 'Cabify Voucher', priceObject,...
```

* I wanted use just vanilla JavaScript without libraries. Only CoffeeScript for the compiler. About this I store the price for each currency. In the practice is better use only a money base and use a converter library to get others currencies. The same with the currency symbol.

* In the Class Item I use as uid the name, but in the real life the name is not the same than the uid. Necessary use a library to get a uid per each Item as well.

* I loaded the data from JavaScript object, but again, in the real life, use a `json` or `yml`, or maybe API endpoint. (Example in `test/data`).


## Copyright

@kikobeats <josefrancisco.verdu@gmail.com>
