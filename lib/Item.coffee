'use strict'

class Item

  ## -- Static -----------------------------------------------------------------

  @clone = (items) ->
    copy = new Item()
    copy.add JSON.parse(JSON.stringify(item)) for item in items.all()
    copy

  ## -- Public -----------------------------------------------------------------

  constructor: ->
    @_all = []

  add: (item) ->
    if item?.discount?.type and not @_isValidDiscount item.discount.type
      throw new Error "Item discount not supported."
    item.count = 0
    @_all.push item
    this

  all: -> @_all
  count: -> @_all.length
  reset: -> item.count = 0 for item in @_all

  get: (itemID) ->
    _item = undefined
    for item in @_all when item.name is itemID
      _item = item
      break
    _item

  isValid: (itemID) ->
    item = @get(itemID)
    Boolean(item)

  increment: (itemID, quantity=1) ->
    item = @get(itemID)
    item.count += quantity

  getSubTotal: (itemID, currency)->
    item = @get(itemID)
    subtotal = item.count * item.price[currency]
    return @_applyDiscount item, subtotal, currency if @_isDiscountAvailable item
    subtotal

  ## -- Private ----------------------------------------------------------------

  _TYPE_DISCOUNTS: ['free units', 'bulk purchases']
  _isValidDiscount: (discount) -> @_TYPE_DISCOUNTS.indexOf(discount) isnt -1

  _isDiscountAvailable: (item) ->
    return false unless item.discount?
    return false if item.count < item.discount.rule.minimum
    true

  _applyDiscount: (item, subtotal, currency) ->
    switch item.discount.type
      when 'free units'
        # count the number of free units and apply discount
        unitsToDiscount = Math.floor item.count / item.discount.rule.minimum
        subtotal = (item.count - unitsToDiscount) * item.price[currency]
      when 'bulk purchases'
        # offer discount per unit
        base = item.count * item.price[currency]
        subtotal = base * (1 - item.discount.rule.free)
    subtotal

## -- Exports ------------------------------------------------------------------

exports = module.exports = Item
