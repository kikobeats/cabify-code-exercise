'use strict'

## -- Dependencies -------------------------------------------------------------

Item = require './Item'

class Checkout

  ## -- Public -----------------------------------------------------------------

  constructor: (options) ->
    unless options?.items?
      throw new Error "Checkout Pricing table is required."

    @items = Item.clone options.items
    delete options.items

    @[option] = options[option] for option of options
    @currency ?= @_DEFAULT.CURRENCY
    @decimals ?= @_DEFAULT.DECIMALS

  add: (itemID) ->
    itemID = itemID.toUpperCase()
    unless @items.isValid(itemID)
      throw new Error "Checkout item '#{itemID}' is not a valid item."
    @items.increment(itemID)
    this

  total: ->
    total = 0
    total += @items.getSubTotal(item.name, @currency) for item in @items.all()
    total.toFixed(@decimals) + @_getCurrencySymbol()

  count: (itemID) ->
    return @items.get(itemID).count if itemID?
    count = 0
    count += item.count for item in @items.all()
    count

  reset: -> @items.reset()

  ## -- Private ----------------------------------------------------------------

  _DEFAULT:
    CURRENCY: 'euro'
    DECIMALS: 2

  _getCurrencySymbol: ->
    switch @currency
      when 'euro'
        '€'
      else
        throw new Error "Checkout Currency Symbol doesn't found."

  _getTotal: ->
    total = 0
    for item of @items
      subtotal = Item.getTotal(item, @items[item])
      total += subtotal

## -- Exports ------------------------------------------------------------------

exports = module.exports = Checkout
