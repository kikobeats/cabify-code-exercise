## -- Dependencies -------------------------------------------------------------

Checkout = require '../lib/Checkout'
Item     = require '../lib/Item'
should   = require 'should'

## -- Tests --------------------------------------------------------------------

console.log """\n
RULES:
  * The marketing department believes in 2-for-1 promotions
  (buy two of the same product, get one free),
  and would like for there to be a 2-for-1 special on VOUCHER items.

  * The CFO insists that the best way to increase sales is with discounts
  on bulk purchases (buying x or more of a product, the price of
  that product is reduced), and demands that
  if you buy 3 or more TSHIRT items, the price per unit should be 19.00€.
"""

console.log """\n
TABLE OF PRICES:
  Code         | Name                |  Price
  -------------------------------------------------
  VOUCHER      | Cabify Voucher      |   5.00€
  TSHIRT       | Cabify T-Shirt      |  20.00€
  MUG          | Cabify Coffee Mug   |   7.50€
"""

console.log """\n
EXAMPLES:
  Items: VOUCHER, TSHIRT, MUG
  Total: 32.50€

  Items: VOUCHER, TSHIRT, VOUCHER
  Total: 25.00€

  Items: TSHIRT, TSHIRT, TSHIRT, VOUCHER, TSHIRT
  Total: 81.00€

  Items: VOUCHER, TSHIRT, VOUCHER, VOUCHER, MUG, TSHIRT, TSHIRT
  Total: 74.50€
"""

describe 'Checkout ::', ->

  before  ->

    @voucher =
      name: 'VOUCHER'
      description: 'Cabify Voucher'
      price: euro: 5.00
      discount:
        type: 'free units'
        rule: minimum: 2, free: 1

    @tshirt =
      name: 'TSHIRT'
      description: 'Cabify T-Shirt'
      price: euro: 20.00
      discount:
        type: 'bulk purchases'
        rule: minimum: 3, free: 0.05

    @mug =
      name: 'MUG'
      description: 'Cabify Coffee Mug'
      price: euro: 7.50

    @items = [@voucher, @tshirt, @mug]
    @items_a = new Item().add(@voucher).add(@tshirt).add(@mug)

  it 'create a new Checkout without pricing table throw a error', ->
    (->
      new Checkout()
    ).should.throw("Checkout Pricing table is required.")

  it 'create a new Checkout providing pricing table and default currency', ->
    @checkout = new Checkout items: @items_a, currency: null
    @checkout.items.all().should.eql @items
    @checkout.items.count().should.eql @items.length
    @checkout.currency.should.eql 'euro'
    @checkout.count().should.eql 0

  it 'try to add a item that is not valid',  ->
    (=>
      @checkout.add('TEST')
    ).should.throw("Checkout item 'TEST' is not a valid item.")

  it 'add one item and get total', ->
    @checkout.add('VOUCHER')
    @checkout.items.get('VOUCHER').count.should.eql 1
    @checkout.count().should.eql 1
    @checkout.total().should.eql "5.00€"

  it "add one item more for get 'free units' discount", ->
    @checkout.add('VOUCHER')
    @checkout.count('VOUCHER').should.eql 2
    @checkout.count().should.eql 2
    @checkout.total().should.eql "5.00€"

  it "add two item more for get 'free units' x2 discount", ->
    @checkout.add('VOUCHER').add('VOUCHER')
    @checkout.count('VOUCHER').should.eql 4
    @checkout.count().should.eql 4
    @checkout.total().should.eql "10.00€"

  it "add three items for get 'bulk purchases' discount", ->
    @checkout.reset()
    @checkout.add('TSHIRT').add('TSHIRT').add('TSHIRT')
    @checkout.count().should.eql 3
    @checkout.count('TSHIRT').should.eql 3
    @checkout.total().should.eql "57.00€"

  it 'using multiple instances of Checkout\'s', ->
    @checkout_a = new Checkout items: @items_a, currency: null
    @checkout_b = new Checkout items: @items_a, currency: null

    @checkout_a.add('VOUCHER').add('TSHIRT').add('MUG')
    @checkout_a.count().should.eql 3
    @checkout_b.count().should.eql 0
    @checkout_a.total().should.eql "32.50€"
    @checkout_b.add('VOUCHER').add('TSHIRT').add('MUG')
    @checkout_a.count().should.eql 3
    @checkout_b.count().should.eql 3
    @checkout_b.total().should.eql "32.50€"

  describe 'Examples ::', ->

    it 'buy VOUCHER, TSHIRT, MUG cost 32.50€', ->
      @checkout = new Checkout items: @items_a, currency: null
      @checkout.add('VOUCHER').add('TSHIRT').add('MUG')
      @checkout.total().should.eql "32.50€"

    it 'buy VOUCHER, TSHIRT, VOUCHER cost 25.00€', ->
      @checkout = new Checkout items: @items_a, currency: null
      @checkout.add('VOUCHER').add('TSHIRT').add('VOUCHER')
      @checkout.total().should.eql "25.00€"

    it 'buy TSHIRT, TSHIRT, TSHIRT, VOUCHER, TSHIRT cost 81.00€', ->
      @checkout = new Checkout items: @items_a, currency: null
      @checkout
        .add('TSHIRT').add('TSHIRT').add('TSHIRT')
        .add('VOUCHER').add('TSHIRT')
      @checkout.total().should.eql "81.00€"

    it 'buy VOUCHER, TSHIRT, VOUCHER, VOUCHER, MUG, TSHIRT, TSHIRT cost 74.50€', ->
      @checkout = new Checkout items: @items_a, currency: null
      @checkout
        .add('VOUCHER').add('TSHIRT').add('VOUCHER')
        .add('VOUCHER').add('MUG').add('TSHIRT').add('TSHIRT')
      @checkout.total().should.eql "74.50€"
